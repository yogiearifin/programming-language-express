const express = require("express");
const router = express.Router();
const programmingLanguanges = require("../services/programmingLanguages");

router.get("/", async (req, res, next) => {
  try {
    res.json(await programmingLanguanges.getMultiple(req.query.page));
  } catch (err) {
    console.log("errror while getting programming languanges", err.message);
    next(err);
  }
});

module.exports = router;
