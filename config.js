require("dotenv").config();

const config = {
  db: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
  },
  listPerPage: process.env.LIST_PER_PAGE || 5,
};

module.exports = config;

console.log(
  config.db.host !== undefined &&
    config.db.user !== undefined &&
    config.db.password !== undefined &&
    config.db.database !== undefined
    ? "env ok"
    : "env  not ok"
);